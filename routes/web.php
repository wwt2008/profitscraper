<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});
Route::post('/data/category', 'ScraperController@scrapeCategory');
Route::get('/data/get', 'ScraperController@getResArr');
Route::post('/data/categories', 'ScraperController@scrapeCategories');

Route::post('/data/save', 'ProjectController@save')->name('project.save');
Route::get('/settings', 'ProjectController@settings')->name('project.settings');
Route::get('/test', 'ProjectController@test')->name('project.test');

