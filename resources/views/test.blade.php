@extends('layout')

@section('content')
                <div class="container-fluid">
                    <div class="card mb-3">
                        <div class="card-header">
                            <i class="fas fa-fw fa-tachometer-alt"></i>
                            Test Scraper</div>
                        <div class="card-body">

                            <div class="form-group">

                                <div class="form-label-group">
                                    <input type="text" id="inputUrl" class="form-control" placeholder="URL"
                                           value="{{$project->url}}"
                                           autofocus="autofocus" readonly>
                                    <label for="inputUrl">URL</label>
                                    <a class="btn btn-info form-control" id="linkUrl" href="{{$project->url}}" target="_blank">Open the page in new tab</a>
                                </div>
                            </div>


                            <input type="hidden" id="inputWord" value="{{$project->word}}">
                            <a id="get-cats-btn" class="btn btn-primary btn-block" href="#">Get subcategories list</a>
                            <div id="cats-res" class="card-body">

                            </div>
                            <p style="display: none" id="get-res-p">Click subcategory to get a number of occurrencces of "{{$project->word}}"</p>
                            <div id="scrape-res" class="card-body">

                            </div>
                        </div>
                        <div class="card-footer small text-muted"></div>
                    </div>
                </div>
                <script>var page = "page-test"</script>
                <!-- /.container-fluid -->
@endsection