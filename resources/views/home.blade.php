@extends('layout')

@section('content')
                <div class="container-fluid">
                    <div class="card mb-3">
                        <div class="card-header">
                            <i class="fas fa-fw fa-home"></i>
                            Home</div>
                        <div class="card-body">
                            <p>
                                Hi, this is just a simple Laravel (PHP, Guzzle, Goutte) based Web scraper which can crawl the
                                category pages of an SHOPBOP.COM website, and count how many occurences of a word
                                are present on each category.
                            </p>
                            <p>
                                First, click Project settings on the left sidebar and change (if you want) URL of the category,
                                word you want to count, proxies and email (they could be saved in db using button Save settings).
                            </p>
                            <p>
                                After that you can click Test the scraper on the left sidebar and check how it works. Clcik
                                Get subcategories list button, and after the list will be loaded (may be in Russian), click
                                any subcategory in the list to scrape it and get a report.
                            </p>
                            <p class="font-weight-bold">The scraper was created just to show my experience in developing of the web scraper based on Laravel framework</p>
                        <p>Working versions of my scrapers use queues (usually Beanstalkd) as a great way to take scraping task out of the user-flow and put them in the background.</p>
                        </div>
                        <div class="card-footer small text-muted"></div>
                    </div>
                </div>
                <script>var page = "page-home"</script>
    @endsection