<ul class="sidebar navbar-nav">
    <li class="nav-item page-home">
        <a class="nav-link" href="/">
            <i class="fas fa-fw fa-home"></i>
            <span>Home</span>
        </a>
    </li>
    <li class="nav-item page-settings">
        <a class="nav-link" href="/settings">
            <i class="fas fa-fw fa-wrench"></i>
            <span>Project settings</span>
        </a>
    </li>
    <li class="nav-item page-test">
        <a class="nav-link" href="/test">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Test scraper</span>
        </a>
    </li>
</ul>