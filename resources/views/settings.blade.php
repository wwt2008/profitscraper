@extends('layout')

@section('content')
                <div class="container-fluid">
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{ $message }}</strong>
                        </div>
                    @elseif($message = Session::get('warning'))
                        <div class="alert alert-warning alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{ $message }}</strong>
                        </div>
                    @endif
                    <div class="card mb-3">
                        <div class="card-header">
                            <i class="fas fa-fw fa-wrench"></i>
                            Project settings</div>
                        <div class="card-body">
                            <form method="post" action="{{ route('project.save') }}" enctype="multipart/form-data">
                                {{ csrf_field() }}
                            <div class="form-group">
                                <div class="form-label-group">
                                    <input type="text" id="inputWord" class="form-control" placeholder="Word"
                                           value="{{isset($project->word) ? $project->word : ''}}"  name="word"
                                           autofocus="autofocus" required>
                                    <label for="inputWord">Word</label>
                                </div>
                            </div>
                            <div class="form-group">

                                <div class="form-label-group">
                                    <input type="text" id="inputUrl" class="form-control" placeholder="URL"
                                           value="{{isset($project->url) ? $project->url : ''}}"  name="url"
                                           autofocus="autofocus" required>
                                    <label for="inputUrl">URL of Shopbop category</label>
                                    <a class="btn btn-info form-control" id="linkUrl" href="{{isset($project->url) ? $project->url : ''}}" target="_blank">Open the page in new tab</a>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-label-group">
                                    <input type="email" id="inputEmail" class="form-control" placeholder="Email address"
                                           value="{{isset($project->email) ? $project->email : ''}}"  name="email"
                                           autofocus="autofocus" required>
                                    <label for="inputEmail">Email address for report</label>
                                </div>
                            </div>
                            <div class="form-group">
                                    <label for="inputProxies">Proxies (one proxy in one row)</label>
                                    <textarea name="proxies" rows="5" id="inputProxies" class="form-control">{{isset($project->proxies) ? $project->proxies : ''}}</textarea>
                            </div>

                            <input type="submit" id="save-settings-btn" class="btn btn-primary btn-block" value="Save settings">
                            </form>
                        </div>
                        <div class="card-footer small text-muted"></div>
                    </div>
                </div>
                <script>var page = "page-settings"</script>
 @endsection