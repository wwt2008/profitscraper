// Get scraped data

$(document).on('click', 'a.cat-link',  function(event) {
    event.preventDefault();
    console.log('#cats-list li a');
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var url = $(this).attr('href');
    var word = $('#inputWord').val();
    console.log('click', url, word);
        $.ajax({
        url:'/data/category',
        type: 'POST',
        dataType: 'html',
        data:{url:url, word:word},
        beforeSend: function() {
            $('#scrape-res').empty();
            $('#scrape-res').addClass('loader');
        },
        success : function (response) {
            console.log('response', response)
            $('#scrape-res').removeClass('loader');
            $('#scrape-res').html(response);
        }

    })
});
$('#get-cats-btn').on('click',  function(event) {
    event.preventDefault();
    console.log('click');
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var url = $('#inputUrl').val();

    function isUrlValid(url) {
        return /^(https?|s?ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(url);
    }
    if (isUrlValid(url))
        $.ajax({
            url:'/data/categories',
            type: 'POST',
            dataType: 'html',
            data:{url:url},
            beforeSend: function() {
                $('#cats-res').empty();
                $('#get-res-p').hide();
                $('#cats-res').addClass('loader');
            },
            success : function (response) {
                console.log('response', response)
                $('#cats-res').removeClass('loader');
                $('#get-res-p').show();
                $('#get-cats-btn').removeClass('disabled');
                $('#cats-res').html(response);
            }

        })
    else alert('Url error')
});
$('#inputUrl').on('input', function(e){
    e.preventDefault();
    console.log($(this).val())
    $('#linkUrl').attr('href', $(this).val())
});
console.log('page',page);

$('ul.sidebar .'+page).first().addClass('active');
