<?php

namespace App\Http\Controllers;

use App\Http\Crawler\Crawler as Crawler;
use Illuminate\Http\Request;


class ScraperController extends Controller
{

    function scrapeCategoriesOld(Request $request)
    {
        $url = $request->input('url');
        $client = new Crawler(['url' => $url]);
        $res = $client->createRequest();

        $crawler = $res['crawler'];
        $nodeArr = $crawler->filterXPath('//div[@id="categories"]/li/a[contains(@data-cs-name,"top-nav-clothing")]/following-sibling::div/div/section[1]/ul/li/a')->each(function ($node, $i) {
            $name = $node->attr('data-cs-name');
            $arr = ['top-nav-clothing-clothing-AllClothing'];
             if(!in_array($name, $arr)) return $node;
        });
        $notNulls = array_filter($nodeArr, function($value) { return $value !== null; });
        $res = [];
        foreach ($notNulls as $notNull){
             $res[] =
                 [
                     'name' => trim(str_replace('top-nav-clothing-clothing-','', $notNull->attr('data-cs-name'))),
                     'link' => 'https://www.shopbop.com'.$notNull->attr('href')
                 ];
        }

        return $this->arrToList($res);
    }
    function scrapeCategories(Request $request)
    {
        $url = $request->input('url');

        $client = new Crawler(['url' => $url]);
        $res = $client->createRequest();

        $crawler = $res['crawler'];
        $nodeArr = $crawler->filterXPath('//ul[@class="leftNavCategory"]/li/a')->each(function ($node, $i) {
            if($i>0) return $node;
        });

        $notNulls = array_filter($nodeArr, function($value) { return $value !== null; });
        $res = [];
        foreach ($notNulls as $notNull){
            $res[] =
                [
                    'name' => $notNull->text(),
                    'link' => 'https://www.shopbop.com'.$notNull->attr('href')
                ];
        }

        return $this->arrToList($res);
    }
    function arrToList($arr){
        $list = '<ul id="cats-list">';
        foreach($arr as $element){
            $list .= '<li><a target="_blank" class="cat-link '.$element['name'].'" href="'.$element['link'].'">'.ucfirst($element['name']).'</a></li>';
        }
        $list .= '</ul>';
        return $list;
    }
    function scrapeCategory(Request $request)
    {

        $url = $request->input('url');
        $word = $request->input('word');
        $client = new Crawler(['url' => $url]);
        $res = $client->createRequest();

        $crawler = $res['crawler'];
        $nodeArr = $crawler->filterXPath('//div[@class="product-list"]//div[@class="brand"]')->each(function ($node, $i) {
            return trim(strtolower($node->text()));
        });

        $itemsCountText = $crawler->filterXPath('//span[@id="product-count"]')->text();
        $itemsCount = 0;
        if (preg_match('/(\d+)/',$itemsCountText, $match)){
            $itemsCount = $match[1];
        };
        //dump($nodeArr);

        $count = ceil ($itemsCount/100);
        //dump($count);
        for ($i=1; $i < $count; $i++){

            $pageUrl = $url.'?baseIndex='.($i*100);
            //dump($pageUrl);
            $client = new Crawler(['url' => $pageUrl]);
            $res = $client->createRequest();
            $crawler = $res['crawler'];
            $nodeArrNew = $crawler->filterXPath('//div[@class="product-list"]//div[@class="brand"]')->each(function ($node, $i) {
                return trim(strtolower($node->text()));
            });
            //dump($nodeArrNew);
            $nodeArr = array_merge($nodeArr, $nodeArrNew);
        }
        $nodeArrUnique = array_count_values($nodeArr);
        $number = 0;

        $word = trim(strtolower($word));
        foreach ($nodeArr as $node){
            //echo '<p>'.$node.'</p>';
            if ($node == $word) {
                //echo '<h2>'.$node.'='.$word.'</h2>';
                $number++;
            }
        }
        echo '<h2>Scraped: '.$count.' pages, '.count($nodeArr).' products</h2>';
        echo '<h3>Word: '.$word.' occurences, '.$number.' times</h3>';
        die($this->getResTable($nodeArrUnique, $word));
    }

    //Convert scraped data from array to html table
    function getResTable($resArr, $word){
        $table = '<table class="table table-striped"><tr><th>Brand</th><th>Occurrences</th></tr>';
        foreach ($resArr as $key => $value) {
                $class = $key == $word ? ' style="background-color: #fdd" ' : '';
                $table .= '<tr'.$class.'><td>' . ucfirst($key) . '</td><td>' . $value . '</td></tr>';

        }
        $table .= '</table>';
        return $table;
    }
    function getResTableNew($resArr, $word){
        $table = '<table class="table table-striped"><tr><th>Brand</th></tr>';
        foreach ($resArr as $key => $value) {

            $table .= '<tr><td>' . $value . '</td></tr>';

        }
        $table .= '</table>';
        return $table;
    }
}
