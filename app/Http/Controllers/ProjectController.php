<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;

class ProjectController extends Controller
{
    /**
     * @param Request $request
     */
    function save(Request $request)
    {
        $projects = Project::all();
        if ($projects->count()>0){
            $project = Project::findOrFail(1);
        }
        else $project = new Project();

        $project->url = $request->input('url');
        $project->word = $request->input('word');
        $project->email = $request->input('email');
        $project->proxies = $request->input('proxies');

        $project->save();

        return back()->with('success','Saved');
    }
    function settings()
    {
        $project = Project::findOrFail(1);
        return view('settings', [
            'project'   => $project,
        ]);
    }
    function test()
    {
        $project = Project::findOrFail(1);
        return view('test', [
            'project'   => $project,
        ]);
    }
}
